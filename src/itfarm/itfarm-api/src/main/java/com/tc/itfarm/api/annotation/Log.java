package com.tc.itfarm.api.annotation;

import java.lang.annotation.*;

/**
 * Created by wangdongdong on 2016/8/11.
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Log {
    String type() default "";
}
