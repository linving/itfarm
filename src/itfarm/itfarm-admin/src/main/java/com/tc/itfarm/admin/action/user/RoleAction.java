package com.tc.itfarm.admin.action.user;

import com.tc.itfarm.api.common.JsonMessage;
import com.tc.itfarm.api.model.Page;
import com.tc.itfarm.api.model.PageInfo;
import com.tc.itfarm.api.model.PageList;
import com.tc.itfarm.api.util.DateUtils;
import com.tc.itfarm.api.util.EncoderUtil;
import com.tc.itfarm.api.util.StringLengthUtil;
import com.tc.itfarm.api.util.StringUtil;
import com.tc.itfarm.model.Role;
import com.tc.itfarm.model.ext.Tree;
import com.tc.itfarm.service.PrivilegeService;
import com.tc.itfarm.service.RolePrivilegeService;
import com.tc.itfarm.service.RoleService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2016/8/14.
 */
@Controller
@RequestMapping("/role")
public class RoleAction {

    private static final Logger logger = LoggerFactory.getLogger(RoleAction.class);
    @Resource
    private RoleService roleService;
    @Resource
    private PrivilegeService privilegeService;
    @Resource
    private RolePrivilegeService rolePrivilegeService;

    @RequestMapping("list")
    public String list() {
        return "admin/role/role";
    }

    /**
     * 角色管理列表
     *
     * @param page
     * @param rows
     * @param sort
     * @param order
     * @return
     */
    @RequestMapping(value = "/dataGrid", method = RequestMethod.POST)
    @ResponseBody
    public Object dataGrid(String name, String startDate, String endDate, Integer page, Integer rows, String sort, String order) {
        PageInfo pageInfo = new PageInfo(page, rows);
        Map<String, Object> condition = new HashMap<String, Object>();

        if (StringUtils.isNotBlank(name)) {
            condition.put("name", name);
        }
        pageInfo.setCondition(condition);
        PageList<Role> rolePageList = roleService.selectByPageList(name, DateUtils.strToDate(startDate), DateUtils.strToDate(endDate), new Page(page, rows));
        pageInfo.setRows(rolePageList.getData());
        pageInfo.setTotal(rolePageList.getPage().getTotalRecords());
        return pageInfo;
    }

    @RequestMapping("addUI")
    public String addUI() {
        return "admin/role/roleAdd";
    }

    @RequestMapping("editUI")
    public String editUI(ModelMap model, @RequestParam(value = "id", required = true) Integer id) {
        Role role = roleService.select(id);
        model.addAttribute(role);
        return "admin/role/roleEdit";
    }

    @RequestMapping("save")
    @ResponseBody
    public JsonMessage save(Role role) {
        Integer result = roleService.save(role);
        return JsonMessage.toResult(result, JsonMessage.STATUS_SUCCESS_MSG, "保存失败");
    }

    @RequestMapping(value = "delete", method = RequestMethod.POST)
    @ResponseBody
    public JsonMessage delete(@RequestParam(value = "id", required = true) Integer id) {
        Integer result = roleService.delete(id);
        return JsonMessage.toResult(result, JsonMessage.STATUS_SUCCESS_MSG, "删除失败");
    }

    /**
     * 授权页面
     *
     * @param id
     * @param model
     * @return
     */
    @RequestMapping("/grantPage")
    public String grantPage(Long id, ModelMap model) {
        model.addAttribute("id", id);
        return "/admin/role/roleGrant";
    }

    @RequestMapping("selectByRoleId")
    @ResponseBody
    public Object selectByRoleId(@RequestParam(value = "id", required = true) Integer id) {
        return privilegeService.selectByRoleId(id);
    }

    @RequestMapping("/grant")
    public JsonMessage grant(@RequestParam(value = "id", required = true) Integer id,
                        @RequestParam(value = "resourceIds", required = true) String resourceIds) {
        List<Integer> privilegeList = StringUtil.strToNumberList(resourceIds, ",");
        JsonMessage jsonMessage = new JsonMessage();
        try {
            rolePrivilegeService.insertAll(id, privilegeList);
            jsonMessage.setSuccess(true);
            jsonMessage.setResultMessage(JsonMessage.STATUS_SUCCESS_MSG);
            return jsonMessage;
        } catch (SQLException e) {
            jsonMessage.setResultMessage("保存失败");
            jsonMessage.setSuccess(false);
            logger.error(e.getMessage(), e);
            return jsonMessage;
        }
    }

    @RequestMapping("/tree")
    @ResponseBody
    public Object tree() {
        return roleService.selectTree();
    }
}
