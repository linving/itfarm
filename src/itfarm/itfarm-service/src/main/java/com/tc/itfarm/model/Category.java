package com.tc.itfarm.model;

import java.io.Serializable;
import java.util.Date;

public class Category implements Serializable {
    private Integer recordId;

    private Integer parentId;

    private String name;

    private String description;

    private Integer userId;

    private Date createTime;

    private Date modifyTime;

    private static final long serialVersionUID = 1L;

    public Category(Integer recordId, Integer parentId, String name, String description, Integer userId, Date createTime, Date modifyTime) {
        this.recordId = recordId;
        this.parentId = parentId;
        this.name = name;
        this.description = description;
        this.userId = userId;
        this.createTime = createTime;
        this.modifyTime = modifyTime;
    }

    public Category() {
        super();
    }

    public Integer getRecordId() {
        return recordId;
    }

    public void setRecordId(Integer recordId) {
        this.recordId = recordId;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }
}