package com.tc.itfarm.dao;

import com.tc.itfarm.api.model.SingleTableDao;
import com.tc.itfarm.model.Privilege;
import com.tc.itfarm.model.PrivilegeCriteria;

public interface PrivilegeDao extends SingleTableDao<Privilege, PrivilegeCriteria> {
}