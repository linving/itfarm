package com.tc.itfarm.service;

import com.tc.itfarm.api.exception.BusinessException;
import com.tc.itfarm.model.Menu;

import java.util.List;

/**
 * Created by wangdongdong on 2016/8/29.
 */
public interface MenuService extends BaseService<Menu> {

    void save(Menu menu, Integer [] articleIds) throws BusinessException;

    Integer selectMaxOrder(Integer parentId);

}
